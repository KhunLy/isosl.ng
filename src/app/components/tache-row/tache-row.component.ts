import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { Tache } from 'src/app/models/tache';
import { ConfirmBoxComponent } from '../confirm-box/confirm-box.component';

@Component({
  selector: 'app-tache-row',
  templateUrl: './tache-row.component.html',
  styleUrls: ['./tache-row.component.scss']
})
export class TacheRowComponent implements OnInit {

  @Input()
  item: Tache

  @Output()
  onDelete: EventEmitter<Tache>

  constructor(
    // injection de dépendance
    private dialService: NbDialogService
  ) { 
    this.onDelete = new EventEmitter<Tache>();
  }

  ngOnInit(): void {
  }

  delete() {
    let ref = this.dialService.open<ConfirmBoxComponent>(ConfirmBoxComponent, {
      context: { data: this.item.nom }
    });
    ref.onClose.subscribe(reponse => {
      if(reponse) {
        this.onDelete.emit(this.item);
      }
    })
  }

}
