import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Agify } from '../models/agify';
import { Country } from '../models/country';

@Injectable({
  providedIn: 'root'
})
export class AgifyService {

  constructor(
    private http: HttpClient
  ) { }

  search(firstName: string, country: Country) : Observable<Agify>{
    let params = new HttpParams();
    params = params.append('country_id', country.cca2);
    params = params.append('name', firstName);
    return this.http.get<Agify>(environment.agifyApi, {params});
  }
}
