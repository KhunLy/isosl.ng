import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Store } from '@ngxs/store';
import { AddMessage } from '../states/chat.state';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  connection: HubConnection;

  constructor(private store: Store) 
  { 
    const b = new HubConnectionBuilder()
      .withUrl('https://furfooz.somee.com/signalr/chat', {
        accessTokenFactory: () => JSON.parse(sessionStorage.getItem('USER')).token,
      });
    this.connection = b.build();
    this.connection.start().then(() => {
      console.log('Vous êtes bien connecté au hub');
      this.connection.on('newMessage', message => {
        // ajouter le nouveau message dans le state
        // console.log(message);
        this.store.dispatch(new AddMessage(message));
      });
    });
  }

  sendMessage(message: string) {
    this.connection.invoke('sendMessage', { content: message })
  }
}
