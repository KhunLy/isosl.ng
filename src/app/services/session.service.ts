import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private _user$: BehaviorSubject<User>

  public get user$() {
    return this._user$.asObservable();
  }

  public get user() {
    return this._user$.value;
  }

  constructor() { 
    let u = sessionStorage.getItem('USER') 
      ? JSON.parse(sessionStorage.getItem('USER')) 
      : null;
    this._user$ = new BehaviorSubject<User>(u);
  }

  start(user: User) {
    sessionStorage.setItem('USER', JSON.stringify(user));
    this._user$.next(user);
  }

  stop() {
    sessionStorage.removeItem('USER');
    this._user$.next(null);
  }
}
