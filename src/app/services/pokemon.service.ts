import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PokemonDetails } from '../models/pokemon.details';
import { PokemonResults, SimplePokemon } from '../models/pokemon.results';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  count: number;

  constructor(
    private http: HttpClient
  ) { }

  public getAll(offset: number = 0) : Observable<SimplePokemon[]> {
    let params = new HttpParams();
    params = params.append('offset', offset);
    return this.http
      .get<PokemonResults>(environment.pokeapi + '/pokemon', { params })
      .pipe(map(data => {
        this.count = data.count
        return data.results
      }));
  }

  public getDetails(name: string) : Observable<PokemonDetails> {
    return this.http.get<PokemonDetails>(environment.pokeapi + '/pokemon/' + name);
  }
}
