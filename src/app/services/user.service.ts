import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  public getByEmail(email: string) : Observable<any[]> {
    return this.http.get<any[]>('http://localhost:3000/users?email=' + email);
  }

  public get(id: number) {
    return this.http.get<any>('http://localhost:3000/users/' + id)
  }

  public post(value: any) {
    return this.http.post('http://localhost:3000/users', value);
  }

  public login(username: string, password: string) {
    return this.http.post<User>('https://furfooz.somee.com/api/login', {
      username, password
    });
  }
}
