import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, skip, Subject, timeout } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Country } from '../models/country';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  private _countries$: BehaviorSubject<Country[]>

  public get countries$(): Observable<Country[]> {
    return this._countries$.asObservable();
  }

  public get countries(): Country[] {
    return this._countries$.value;
  }

  constructor(
    private http : HttpClient
  ) {
    this.getAll().subscribe(data => this._countries$.next(data));
    this._countries$ = new BehaviorSubject<Country[]>(null);
  }

  public getAll(): Observable<Country[]> {
    return this.http.get<Country[]>(environment.countryApi + '/all')
      .pipe(
        map(c => c.sort((a, b) => a.name.common < b.name.common ? -1 : 1))
      );
  }
}
