import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { Store } from '@ngxs/store';
import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';
import { start } from 'src/app/states/session.state';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  fg: FormGroup
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private toastrService: NbToastrService,
    private session: SessionService,
    private router: Router,
    private store: Store
  ) { }

  ngOnInit(): void {
    this.fg = this.fb.group({
      username: [],
      password: []
    });
  }

  submit() {
    this.userService.login(this.fg.value.username, this.fg.value.password)
      .subscribe({
        next: data => {
          this.router.navigateByUrl('/');
          this.store.dispatch(new start(data));
          // this.session.start(data);
          // sauvegarder l'utilisateur connecté dans le session service
          this.toastrService.info('Bienvenue');
        },
        error: e => { this.toastrService.danger('Bad credentials') }
      });
  }

  preventEnter($event: any) {
    if($event.keyCode === 13) {
      $event.preventDefault()
    }
  }

}
