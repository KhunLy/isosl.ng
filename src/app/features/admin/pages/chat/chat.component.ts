import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Message } from 'src/app/models/message';
import { ChatService } from 'src/app/services/chat.service';
import { ChatState } from 'src/app/states/chat.state';

@Component({
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  message: string;

  @Select(ChatState)
  message$: Observable<Message[]>

  messages: Message[];

  constructor(
    private chatService: ChatService
  ) { }

  ngOnInit(): void {
    this.message$.subscribe(m => this.messages = m);
  }

  submit() {
    this.chatService.sendMessage(this.message);
    this.message = null;
  }

}
