import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbButtonModule, NbInputModule, NbToastrModule } from '@nebular/theme';
import { ChatComponent } from './pages/chat/chat.component';


@NgModule({
  declarations: [
    AdminComponent,
    LoginComponent,
    ChatComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    NbInputModule,
    NbButtonModule,
    //NbToastrModule,
  ]
})
export class AdminModule { }
