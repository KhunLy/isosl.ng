export interface Tache {
    nom: string;
    accomplie: boolean;
}