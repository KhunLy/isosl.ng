import { AbstractControl, AsyncValidatorFn, FormGroup, ValidationErrors, ValidatorFn } from "@angular/forms";
import { map, of } from "rxjs";
import { UserService } from "../services/user.service";

export class CustomValidation {

    static notAfterDate(date: Date) : ValidatorFn {
        return (control: AbstractControl) : ValidationErrors | null => {
            // si c'est ok retourner null
            if(!control.value) {
                return null;
            }
            let value: Date = control.value;
            // sinon retourner un objet
            return value < date ? null : { 'notAfterDateError': date }; 
        }
    }

    static ssnDate(dateName: string, ssnName: string) {
        return (fg : FormGroup) : ValidationErrors | null => {
            console.log(42);
            
            let date: Date = fg.get(dateName).value;
            let ssn: string = fg.get(ssnName).value;
            if(!date || !ssn) return null;
            let regex = new RegExp(/^[0-9]{2}.[0-9]{2}.[0-9]{2}$/);
            let datePart = ssn.substring(0,8);
            if(!datePart.match(regex)) {
                return null;
            }
            let year = datePart.split('.')[0];
            let month = datePart.split('.')[1];
            let day = datePart.split('.')[2];
            if(parseInt(month) - 1 != date.getMonth()) {
                return { ssnDateError: true };
            }
            return null;
        }
    }

    static uniqueEmail(userService: UserService): AsyncValidatorFn {
        return control => {
            let value: string = control.value;
            if(!value) {
                return of(null);
            }
            return userService.getByEmail(value).pipe(map(users => {
                if(users.length === 0) {
                    return null;
                }
                return { 'emailUniqueError' : true };
            }))
        }
    }

}