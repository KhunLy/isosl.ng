import { ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbButtonModule, NbSidebarModule, NbMenuModule, NbInputModule, NbCardModule, NbIconModule, NbToastrModule, NbListItemComponent, NbListModule, NbDialogModule, NbSpinnerModule, NbSelectModule, NbDatepickerModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { Demo1Component } from './pages/demo1/demo1.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Demo2Component } from './pages/demo2/demo2.component';
import { Ex1Component } from './pages/ex1/ex1.component';
import { ToTimerPipe } from './pipes/to-timer.pipe';
import { Demo3Component } from './pages/demo3/demo3.component';
import { Ex2Component } from './pages/ex2/ex2.component';
import { TacheRowComponent } from './components/tache-row/tache-row.component';
import { ConfirmBoxComponent } from './components/confirm-box/confirm-box.component';
import { Demo4Component } from './pages/demo4/demo4.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { Demo4DetailsComponent } from './pages/demo4-details/demo4-details.component';
import { Ex3Component } from './pages/ex3/ex3.component';
import { Demo5Component } from './pages/demo5/demo5.component';
import { FormErrorComponent } from './components/form-error/form-error.component';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { NgxsModule } from '@ngxs/store';
import { SessionState } from './states/session.state';
import { environment } from 'src/environments/environment';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { ChatState } from './states/chat.state';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    Demo1Component,
    Demo2Component,
    Ex1Component,
    ToTimerPipe,
    Demo3Component,
    Ex2Component,
    TacheRowComponent,
    ConfirmBoxComponent,
    Demo4Component,
    LoaderComponent,
    Demo4DetailsComponent,
    Ex3Component,
    Demo5Component,
    FormErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,

    // nécessaire pour le binding 2 ways
    FormsModule,
    // nécessaire pour les formulaires complexes
    ReactiveFormsModule,

    NbThemeModule.forRoot({ name: 'corporate' }),
    NbLayoutModule,
    NbEvaIconsModule,

    NbButtonModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbInputModule,
    NbCardModule,
    NbIconModule,
    NbToastrModule.forRoot(),
    NbListModule,
    NbDialogModule.forRoot({
      closeOnBackdropClick: false
    }),
    NbSelectModule,
    NbSpinnerModule,
    NbDatepickerModule.forRoot(),

    NgxsModule.forRoot([ SessionState, ChatState ], {
      developmentMode: !environment.production
    }),

    NgxsReduxDevtoolsPluginModule.forRoot(),

    // nécessaire pour les connection http
    HttpClientModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
