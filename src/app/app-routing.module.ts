import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsLoggedGuard } from './guards/is-logged.guard';
import { AboutComponent } from './pages/about/about.component';
import { Demo1Component } from './pages/demo1/demo1.component';
import { Demo2Component } from './pages/demo2/demo2.component';
import { Demo3Component } from './pages/demo3/demo3.component';
import { Demo4DetailsComponent } from './pages/demo4-details/demo4-details.component';
import { Demo4Component } from './pages/demo4/demo4.component';
import { Demo5Component } from './pages/demo5/demo5.component';
import { Ex1Component } from './pages/ex1/ex1.component';
import { Ex2Component } from './pages/ex2/ex2.component';
import { Ex3Component } from './pages/ex3/ex3.component';
import { HomeComponent } from './pages/home/home.component';
import { CountryResolver } from './resolvers/country.resolver';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'demo1', component: Demo1Component },
  { path: 'demo2', component: Demo2Component },
  { path: 'demo3', component: Demo3Component },
  { path: 'demo4', component: Demo4Component },
  // url/:param pour un paramètre de route
  { path: 'demo4-details/:name', component: Demo4DetailsComponent },
  { path: 'demo5', component: Demo5Component, resolve: { countries: CountryResolver }
    , canActivate: [ IsLoggedGuard ] },
  { path: 'ex1', component: Ex1Component },
  { path: 'ex2', component: Ex2Component },
  { path: 'ex3', component: Ex3Component, resolve: { countries: CountryResolver } },
  { path: 'admin', loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
