import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toTimer'
})
export class ToTimerPipe implements PipeTransform {

  transform(value: number): string {

    if(!value) {
      return '';
    }

    let milliseconds = value % 1000;
    let seconds = Math.floor(value / 1000) % 60;
    let minutes = Math.floor(value / 60000) % 60;

    return `${('00' + minutes).slice(-2)}:${('00' + seconds).slice(-2)}:${('000' + milliseconds).slice(-3)}`;
  }

}
