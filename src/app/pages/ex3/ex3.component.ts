import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { last } from 'rxjs';
import { Agify } from 'src/app/models/agify';
import { Country } from 'src/app/models/country';
import { AgifyService } from 'src/app/services/agify.service';
import { CountryService } from 'src/app/services/country.service';

@Component({
  templateUrl: './ex3.component.html',
  styleUrls: ['./ex3.component.scss']
})
export class Ex3Component implements OnInit {

  countries: Country[];

  selectedCountry: Country;

  firstName: string;
  agify: Agify;

  constructor(
    private route: ActivatedRoute,
    private agifyService: AgifyService,
  ) { }

  ngOnInit(): void {
    this.route.data
      .subscribe(({countries}) => this.countries = countries);
  }

  search() {
    this.agifyService.search(this.firstName, this.selectedCountry)
      .subscribe(data => { 
        this.agify = data;
        this.firstName = null;
      });
  }

}
