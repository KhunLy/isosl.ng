import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RegisterForm } from 'src/app/forms/register.form';
import { Country } from 'src/app/models/country';
import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';
import { CustomValidation } from 'src/app/validators/custom-validation';

@Component({
  templateUrl: './demo5.component.html',
  styleUrls: ['./demo5.component.scss'],
})
export class Demo5Component implements OnInit {

  fg: FormGroup;
  countries: Country[];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private userService: UserService,
    private http: HttpClient,
  ) { }

  ngOnInit(): void {

    this.http.get('https://furfooz.somee.com/api/values')
      .subscribe(console.log);
    // this.userService.get(1).subscribe(data => {
    //   // essayer de faire mapper les données du formulaire avec les datas
    //   this.fg.patchValue(data);
    // });

    this.countries = this.route.snapshot.data['countries'];
    this.fg = this.fb.group(RegisterForm , { validators: [ CustomValidation.ssnDate('dateDeNaissance', 'ssn') ] });

    let emailControl = this.fg.get('email');
    let natControl = this.fg.get('nationality');
    let ssnControl = this.fg.get('ssn');

    emailControl.addAsyncValidators([CustomValidation.uniqueEmail(this.userService)])

    natControl.valueChanges.subscribe(v => {
      if(v === 'BE') {
        ssnControl.setValidators([Validators.required]);
      }
      else {
        ssnControl.setValidators([]);
        ssnControl.reset();
      } 
    });
  }

  submit() {
    if(this.fg.invalid) return;
    this.userService.post(this.fg.value).subscribe({
      next: () => {
        this.fg.reset()
      }, // en cas de success,
      error: () => {}, // en cas d'erreur
      complete: () => {} // dans tous les cas
    });

    //this.userService.post(this.fg.value).subscribe(() =>{}, () => {}, () => {})
  }

}
