import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './demo3.component.html',
  styleUrls: ['./demo3.component.scss']
})
export class Demo3Component implements OnInit {

  isLogged: boolean = false;

  collection: string[] = ['sucre', 'sel', 'poivre'];

  constructor() { }

  ngOnInit(): void {
  }

}
