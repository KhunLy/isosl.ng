import { Component, OnInit } from '@angular/core';
import { Tache } from 'src/app/models/tache';

@Component({
  templateUrl: './ex2.component.html',
  styleUrls: ['./ex2.component.scss']
})
export class Ex2Component implements OnInit {

  newItem: string;
  items: Tache[];

  constructor() { }

  ngOnInit(): void {
    this.items = [];
  }

  add() {
    //this.items.push(this.newItem);
    this.items = [...this.items, { nom: this.newItem, accomplie: false }];
    this.newItem = null;
  }

  delete(tache: Tache) {
    this.items = this.items.filter(x => x !== tache);
  }

}
