import { Component, OnInit } from '@angular/core';
import { SimplePokemon } from 'src/app/models/pokemon.results';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  templateUrl: './demo4.component.html',
  styleUrls: ['./demo4.component.scss']
})
export class Demo4Component implements OnInit {

  //result$: Observable<any>;
  
  results: SimplePokemon[];

  currentOffset: number = 0;

  constructor(
    private pokemonService: PokemonService
  ) { }

  ngOnInit(): void {
    //this.result$ = this.http.get('https://pokeapi.co/api/v2/pokemon');
    this._loadItems();
  }

  prev() {
    if(this.currentOffset === 0) return;
    this.currentOffset -= 20;
    this._loadItems();
  }

  next() {
    if(this.currentOffset + 20 > this.pokemonService.count ?? 0) return;
    this.currentOffset += 20;
    this._loadItems();
  }

  private _loadItems() {
    this.pokemonService.getAll(this.currentOffset).subscribe(data => {
      this.results = data;
    });
  }

}
