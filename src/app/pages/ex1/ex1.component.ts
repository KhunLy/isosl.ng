import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './ex1.component.html',
  styleUrls: ['./ex1.component.scss']
})
export class Ex1Component implements OnInit {

  timer: any;
  startDate: Date;
  count: number;
  memo: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  start() {
    if(this.timer) return;
    this.startDate = new Date();
    this.timer = setInterval(() => {
      this.count = new Date().getTime() - this.startDate.getTime() + this.memo;
    }, 1);
  }

  stop() {
    clearInterval(this.timer);
    this.timer = null;
    this.memo = this.count;
  }

  reset() {
    this.stop();
    this.memo = 0;
    this.count = 0;
  }

}
