import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Subject, switchMap, takeUntil } from 'rxjs';
import { PokemonDetails } from 'src/app/models/pokemon.details';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  templateUrl: './demo4-details.component.html',
  styleUrls: ['./demo4-details.component.scss']
})
export class Demo4DetailsComponent implements OnInit, OnDestroy {

  // subscription: Subscription
  // destroyed$ est un observable qui resolvera un boolean
  destroyed$: Subject<any>;

  pokemon: PokemonDetails;

  constructor(
    private pokemonService: PokemonService,
    private route: ActivatedRoute,
  ) {
    this.destroyed$ = new Subject<boolean>();
  }

  ngOnInit(): void {

    // est appelé à chaque fois que le composant apparait
    // let name = this.route.snapshot.params['name'];

    // this.route.params.subscribe(data => console.log(data['name'])

    /*this.subscription  =*/ this.route.params.pipe(
      // permet de s"assurer que l'abonnement soit "canceled"
      takeUntil(this.destroyed$),
      map(data => data['name']),
      switchMap(name => this.pokemonService.getDetails(name)),
    ).subscribe(data => this.pokemon = data);

  }

  ngOnDestroy(): void {
    // est appelée à chaque fois que l'on quitte un composant
    // se désabonner à tous les observables
    // this.subscription.unsubscribe();
    // invoquer l'observable avec une la valeur
    this.destroyed$.next(true);
    // indique que l'observable n'emettera plus (casser les souscriptions)
    this.destroyed$.complete();
  }
}
