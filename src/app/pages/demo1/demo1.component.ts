import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.scss']
})
export class Demo1Component implements OnInit {

  nom: string = 'Khun';

  date: Date = new Date();

  prix: number = 42;

  // any type ?
  coord: any = {
    lat: 50.4,
    lng: 4.2
  };

  
  private _entree: string;
  public get entree() : string {
    console.log("la variable est récupéree du vm => la vue");
    return this._entree;
  }
  public set entree(v : string) {
    console.log("la variable est modifiée depuis la vue => la vm");
    this._entree = v;
  }
  

  constructor() { }

  // la methode ngOnInit est appelée à chaque fois que le composant apparait
  ngOnInit(): void {
    // fonction js qui permet d'attendre avant d'executer qqchose
    setTimeout(() => { this.nom = 'Mike' }, 3000);
  }

  

}
