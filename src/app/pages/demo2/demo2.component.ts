import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.scss']
})
export class Demo2Component implements OnInit {


  state: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  toggle(): void {
    this.state = !this.state;
  }

}
