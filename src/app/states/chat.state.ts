import {  Injectable } from "@angular/core";
import { Action, State, StateContext } from "@ngxs/store";
import { Message } from "../models/message";

export class AddMessage{
    static readonly type: string = '[chat] addMessage'
    constructor(public message: Message) {}
}

@State<Message[]>({
    name: 'chat',
    defaults: []
})
@Injectable()
export class ChatState {
    @Action(AddMessage)
    addMessage(ctx: StateContext<Message[]>, action: AddMessage) {
        let state = ctx.getState();
        ctx.setState([...state, action.message]);
    }
}