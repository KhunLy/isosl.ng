import { Injectable } from "@angular/core";
import { Action, State, StateContext } from "@ngxs/store";
import { User } from "../models/user";

// actions
export class start {
    static readonly type = '[session] start';
    constructor(public user: User) {}
}
export class stop {
    static readonly type= '[session] stop';
}

@State<User>({
    name: 'session',
    // initialisation du state
    defaults: sessionStorage.getItem('USER') 
            ? JSON.parse(sessionStorage.getItem('USER'))
            : null
})
@Injectable()
export class SessionState {

    // reducers
    @Action(start)
    startSession(ctx: StateContext<User>, action: start) {
        sessionStorage.setItem('USER', JSON.stringify(action.user));
        ctx.setState(action.user);
    }

    @Action(stop)
    stopSession(ctx: StateContext<User>, action: stop) {
        sessionStorage.removeItem('USER');
        ctx.setState(null);
    }
}