import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { NbDialogService } from '@nebular/theme';
import { LoaderComponent } from '../components/loader/loader.component';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(
    private dialogService: NbDialogService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if(request.url.startsWith('http://localhost:3000/users?email')) {
      return next.handle(request);
    }
    let ref = this.dialogService.open(LoaderComponent);
    return next.handle(request).pipe(finalize(() => {
      ref.close();
    }));
  }
}
