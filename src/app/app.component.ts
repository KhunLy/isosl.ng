import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NbMenuItem, NbMenuService } from '@nebular/theme';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { User } from './models/user';
import { SessionService } from './services/session.service';
import { SessionState, stop } from './states/session.state';

const menuBase = [
  { title: 'Accueil', link: '/home', icon: 'home' },
  { title: 'A Propos', link: '/about', icon: 'star' },
  { title: 'Demos', icon: 'book' , children: [
    { title: 'Demo 1 - Binding', link: '/demo1' },
    { title: 'Demo 2 - Evenements', link: '/demo2' },
    { title: 'Demo 3 - Structural directives', link: '/demo3' },
    { title: 'Demo 4 - Http', link: '/demo4' },
    { title: 'Demo 5 - Reactive Forms', link: '/demo5' },
  ] },
  { title: 'Exos', icon: 'github', children: [
    { title: 'Ex 1 - Timer', link: '/ex1' },
    { title: 'Ex 2 - Check List', link: '/ex2' },
    { title: 'Ex 3 - Genderize', link: '/ex3' },
  ] }
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  items: NbMenuItem[];

  @Select(SessionState)
  user$: Observable<User>;

  constructor(
    private session : SessionService,
    private menuService: NbMenuService,
    private router: Router,
    private store: Store,
  ) {}

  ngOnInit(): void {
    this.user$.subscribe(u => {
      if(u) {
        this.items = [
          ...menuBase,
          { title: 'Se déco', icon: 'power' }
        ];
      }
      else {
        this.items = [
          ...menuBase,
          { title: 'Login', icon: 'power', link: '/admin/login' }
        ]
      }
    });

    this.menuService.onItemClick().subscribe(item => {
      if(item.item.title === 'Se déco') {
        this.router.navigateByUrl('/admin/login');
        //this.session.stop();
        this.store.dispatch(new stop());
      }
    });
  }

  


}
