import { Validators } from "@angular/forms";
import { CustomValidation } from "../validators/custom-validation";

export const RegisterForm : any = {
    'nom': [null, [ Validators.required, Validators.minLength(2) ], [] /*validations asynchones*/ ],
    'prenom': [null, [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
    'nationality': ['BE', [Validators.required]],
    'dateDeNaissance': [null, [CustomValidation.notAfterDate(new Date())]],
    'ssn': [null, []],
    'email': [null, [Validators.required, Validators.email] ]
  } 