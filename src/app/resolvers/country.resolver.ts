import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { first, from, mergeMap, Observable, of, skip, take } from 'rxjs';
import { Country } from '../models/country';
import { CountryService } from '../services/country.service';

@Injectable({
  providedIn: 'root'
})
export class CountryResolver implements Resolve<Country[]> {

  constructor(private countryService: CountryService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<Country[]> {
    if(this.countryService.countries) {
      // of retourne un observable sur base d'une valeur et déclenche l'observable
      return of(this.countryService.countries);
    }
    else {
      return this.countryService.countries$.pipe(skip(1), first());
    }
  }
}
