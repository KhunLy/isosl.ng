import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Select } from '@ngxs/store';
import { map, Observable, tap } from 'rxjs';
import { User } from '../models/user';
import { SessionService } from '../services/session.service';
import { SessionState } from '../states/session.state';

@Injectable({
  providedIn: 'root'
})
export class IsLoggedGuard implements CanActivate {

  @Select(SessionState)
  user$: Observable<User>

  constructor(
    private session: SessionService,
    private router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.user$.pipe(map(u => !!u), tap(connected => {
        if(!connected) {
          this.router.navigateByUrl('/admin/login');
        }
      }));
  }
  
}
