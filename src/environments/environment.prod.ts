export const environment = {
  production: true,
  pokeapi: 'https://pokeapi.co/api/v2',
  countryApi: 'https://restcountries.com/v3.1',
  agifyApi: 'https://api.agify.io',
};
